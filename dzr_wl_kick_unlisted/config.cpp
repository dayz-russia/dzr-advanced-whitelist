class CfgPatches
{
	class dzr_wl_kick_unlisted
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_wl_kick_unlisted
	{
		type = "mod";
		author = "DayZRussia";
		dir = "dzr_wl_kick_unlisted";
		name = "dzr_wl_kick_unlisted";
		dependencies[] = {"Game","World","Mission"};
		class defs
		{
			class gameScriptModule
			{
				files[] = {"dzr_wl_kick_unlisted/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_wl_kick_unlisted/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_wl_kick_unlisted/5_Mission"};
			};
		};
	};
};
